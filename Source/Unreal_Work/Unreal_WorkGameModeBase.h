// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Unreal_WorkGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class UNREAL_WORK_API AUnreal_WorkGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
