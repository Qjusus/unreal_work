// Copyright Epic Games, Inc. All Rights Reserved.

#include "Unreal_Work.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Unreal_Work, "Unreal_Work" );
